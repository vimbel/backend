<?php

namespace Vimbel\Repositories;

use Illuminate\Http\Request;
use Vimbel\Models\Message;

class MessageRepository {

    public function all()
    {
        return Message::all();
    }

    public function whereId($id)
    {
        return Message::find($id);
    }

    public function create($attributes)
    {
        $message = new Message;
        $message->room_id = $attributes['room_id'];
        $message->user_id = $attributes['user_id'];

        if(!array_key_exists('content', $attributes)){
            $message->is_attachment = true;
        }else{
            $message->content = $attributes['content'];
        }

        $message->save();
        
        return $this->whereId($message->id);
    }

    public function delete($id)
    {
        $message = $this->whereId($id);
        $message->delete();

        return $message;
    }
}
