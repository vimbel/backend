<?php

namespace Vimbel\Repositories;

use Illuminate\Http\Request;
use Vimbel\Models\{Room, User};

class RoomRepository {

    public function all()
    {
        return Room::all();
    }

    public function whereId($id)
    {
        return Room::find($id);
    }

    public function whereUserId($id)
    {
        return User::find($id)->teams;
    }

    public function create(Request $request)
    {
        $room = new Room;
        $room->name = $request->name;
        $room->slug = $request->name;
        $user->save();

        return $room;
    }

    public function update(Request $request, $id)
    {
        $room = $this->whereId($id);
        if($request->has('name'))
            $room->name = $request->name;
        $room->save();

        return $room;
    }
}
