<?php

namespace Vimbel\Repositories;

use Illuminate\Http\Request;
use Vimbel\Models\Attachment;

class AttachmentRepository {

    public function all()
    {
        return Attachment::all();
    }

    public function whereId($id)
    {
        return Attachment::find($id);
    }

    public function create($attributes)
    {
        $attachment = new Attachment;
        $attachment->message_id = $attributes['message_id'];
        $attachment->title = $attributes['title'];
        $attachment->url = $attributes['url'];
        $attachment->extension = $attributes['extension'];
        $attachment->save();

        return $attachment;
    }

    public function update($attributes, $id)
    {
        $attachment = $this->whereId($id);

        if(array_has_key('title', $attributes)){
            
            $attachment->title = $attributes['title'];
        }

        $attachment->save();

        return $attachment;
    }
}
