<?php

namespace Vimbel\Repositories;

use Illuminate\Http\Request;
use Vimbel\Models\User;

class UserRepository {

    public function all()
    {
        return User::all();
    }

    public function whereId($id)
    {
        return User::find($id);
    }

    public function create(Request $request)
    {
        $user = new User;

        $user->first_name = $request->first_name;
        if($request->has('last_name'))
            $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        return $user;
    }

    public function update(Request $request, $id)
    {
        $user = $this->whereId($id);

        if($request->has('first_name'))
            $user->last_name = $request->first_name;
        if($request->has('last_name'))
            $user->last_name = $request->last_name;
        if($request->has('email'))
            $user->last_name = $request->email;
        $user->save();

        return $user;
    }
}
