<?php

namespace Vimbel\Repositories;

use Illuminate\Http\Request;
use Vimbel\Models\Team;

class TeamRepository {

    public function all()
    {
        return Team::all();
    }

    public function whereId($id)
    {
        return Team::find($id);
    }

    public function create(Request $request)
    {
        $team = new Team;
        $team->name = $request->name;
        $team->slug = $request->name;
        $user->save();

        return $team;
    }

    public function update(Request $request, $id)
    {
        $team = $this->whereId($id);
        if($request->has('name'))
            $team->name = $request->name;
        $team->save();

        return $team;
    }
}
