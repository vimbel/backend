<?php

namespace Vimbel\Http\Controllers;

use Illuminate\Http\Request;
use Vimbel\Http\Requests;
use Vimbel\Repositories\MessageRepository;

class MessagesController extends Controller
{
    protected $messages;

    public function __construct(MessageRepository $messages)
    {
        $this->messages = $messages;
    }

    public function index()
    {
        return $this->messages->all();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'room_id' => 'required|int',
            'user_id' => 'required|int',
            'content' => 'string'
        ]);

        $newMessage = [
            'room_id' => $request->room_id,
            'user_id' => $request->user_id,
            'content' => $request->content
        ];

        return $this->messages->create($newMessage);
    }

    public function destroy($id)
    {
        return $this->messages->delete($id);
    }

    public function forTeam($id)
    {
        return $this->messages->forTeam($id);
    }
}
