<?php

namespace Vimbel\Http\Controllers;

use Illuminate\Http\Request;
use Vimbel\Http\Requests;
use Vimbel\Repositories\RoomRepository;

class RoomsController extends Controller
{
    public function __construct(RoomRepository $rooms)
    {
        $this->rooms = $rooms;
    }

    public function index()
    {
        return $this->rooms->all();
    }

    public function store(Request $request)
    {
        $this->rooms->create($request);
    }

    public function update(Request $request, $id)
    {
        return $this->rooms->update($request, $id);
    }

    public function destroy($id)
    {
        //
    }
}
