<?php

namespace Vimbel\Http\Controllers;

use Illuminate\Http\Request;
use Vimbel\Http\Requests;
use Vimbel\Repositories\{UserRepository, TeamRepository};

class UsersController extends Controller
{
    protected $users;
    protected $teams;

    public function __construct(UserRepository $users, TeamRepository $teams)
    {
        $this->users = $users;
        $this->teams = $teams;
    }

    public function index()
    {
        return response()->json($this->users->all());
    }

    public function show($id)
    {
        return response()->json($this->users->whereId($id));
    }

    public function update(Request $request, $id)
    {
        $this->valiidate($request, [
            'first_name' => 'string',
            'last_name' => 'string',
            'email' => 'email'
        ]);

        return response()->json($this->users->update($request, $id));
    }

}
