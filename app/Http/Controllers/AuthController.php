<?php

namespace Vimbel\Http\Controllers;

use Illuminate\Http\Request;
use Vimbel\Http\Requests;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Vimbel\Models\User;
use Auth;

class AuthController extends Controller
{
    public function authenticate(Request $request)
    {
        try {
            if (! $user = Auth::attempt($request->only('email', 'password'))) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $user = Auth::user();

        $payload = [
            'first_name' => $user->first_name,
            'last_name' => $user->last_name
        ];

        $token = JWTAuth::fromUser($user, $payload);

        return response()->json($token);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'string',
            'email' => 'required|email',
            'password' => 'required|string'
        ]);

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->first_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return response()->json($user);
    }

    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);

        JWTAuth::invalidateToken($request->token);
    }
}
