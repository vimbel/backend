<?php

namespace Vimbel\Http\Controllers;

use Illuminate\Http\Request;
use Vimbel\Repositories\{TeamRepository, UserRepository};
use Vimbel\Models\Team;

class AssetsController extends Controller
{
	protected $teams;
	protected $users;

	public function __construct(TeamRepository $teams, UserRepository $users)
	{
		$this->teams = $teams;
		$this->users = $users;
	}

	public function webappAsset(Request $request)
    {
    	$this->validate($request, [
	        'team_id' => 'integer|required',
	        'user_id' => 'integer|required'
        ]);

        $user = $this->users->whereId($request->user_id);
        $team = $this->teams->whereId($request->team_id);

        $asset = [
            'rooms' => $team->rooms,
            'messages' => $team->messages,
            'team' => $team,
            'teams' => $user->teams,
            'user' => $user
        ];

        return response()->json($asset);
    }
}
