<?php

namespace Vimbel\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Vimbel\Repositories\{AttachmentRepository, MessageRepository, TeamRepository};

class UploadsController extends Controller
{
	protected $attachments;
	protected $messages;
	protected $teams;

	public function __construct(AttachmentRepository $attachments, MessageRepository $messages, TeamRepository $teams)
	{
		$this->attachments = $attachments;
		$this->messages = $messages;
		$this->teams = $teams;
	}

    public function attachment(Request $request)
    {
    	$this->validate($request, [
	    	'file' => 'required|file|max:100000',
	    	'team_id' => 'required|int|',
	    	'user_id' => 'required|int|',
	    	'room_id' => 'required|int|',
	    	'title' => 'string'
	    ]);

    	$team = $this->teams->whereId($request->team_id);

    	$newMessage = [
    		'room_id' => $request->room_id,
			'user_id' => $request->user_id
    	];

    	$message = $this->messages->create($newMessage);
	    $file = $request->file('file');

	    if($request->has('title')){
	    	$url = $file->storeAs('uploads/attachments/' . $team->uuid, str_slug($request->title) . $file->getClientOriginalExtension(), 's3');
	    }else{
	    	$url = $file->store('uploads/attachments/' . $team->uuid, 's3');
	    }

		$newAttachment = [
			'message_id' => $message->id,
			'team_id' => $request->team_id,
			'title' => $request->title,
			'extension' => $file->guessExtension(),
			'url' => $url
		];

		$attachment = $this->attachments->create($newAttachment);

    	return response()->json($this->messages->whereId($message->id));
    }
}
