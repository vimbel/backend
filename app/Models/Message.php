<?php

namespace Vimbel\Models;

use Illuminate\Database\Eloquent\{Model, SoftDeletes};

class Message extends Model
{
	use SoftDeletes;

    protected $appends = ['room_name'];
    protected $dates = ['created_at', 'deleted_at', 'updated_at'];
    protected $hidden = ['user_id', 'deleted_at', 'updated_at'];
    protected $with = ['attachment', 'user'];
	
    public function attachment()
    {
        return $this->hasOne(Attachment::class);
    }
    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getRoomNameAttribute()
    {
        return Room::find($this->attributes['room_id'])->name;
    }
}
