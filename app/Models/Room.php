<?php

namespace Vimbel\Models;

use Illuminate\Database\Eloquent\{Model, SoftDeletes};

class Room extends Model
{
    use SoftDeletes;
    
    protected $hidden = ['deleted_at', 'updated_at', 'pivot'];
    
    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
