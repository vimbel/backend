<?php

namespace Vimbel\Models;

use Illuminate\Foundation\Auth\user as Authenticatable;

class User extends Authenticatable
{
    protected $fillable = ['first_name', 'last_name', 'email', 'password'];
    protected $visible = ['id', 'avatar_path', 'first_name', 'last_name'];

    public function rooms()
    {
        return $this->belongsToMany(Room::class);
    }

    public function teams()
    {
        return $this->belongsToMany(Team::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }
}
