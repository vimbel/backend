<?php

namespace Vimbel\Models;

use Illuminate\Database\Eloquent\{Model, SoftDeletes};

class Team extends Model
{
	use SoftDeletes;
	
    protected $visible = ['id', 'name'];

    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function messages()
    {
        return $this->hasManyThrough(Message::class, Room::class);
    }
}
