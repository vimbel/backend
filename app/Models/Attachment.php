<?php

namespace Vimbel\Models;

use Illuminate\Database\Eloquent\Model;
use Vimbel\Models\Message;

class Attachment extends Model
{
    public function message()
    {
    	return $this->belongsTo(Message::class);
    }
}
