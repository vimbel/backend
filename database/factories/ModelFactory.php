<?php

use Vimbel\Models\{Room, Team, User};

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Vimbel\Models\User::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Vimbel\Models\Team::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'slug' => $faker->name
    ];
});

$factory->define(Vimbel\Models\Room::class, function (Faker\Generator $faker) {
    $count = Team::all()->count() - 1;
    return [
        'name' => $faker->company,
        'team_id' => rand(1, $count),
        'description' => $faker->sentence
    ];
});

$factory->define(Vimbel\Models\Message::class, function (Faker\Generator $faker) {
    $ucount = User::all()->count() - 1;
    $rcount = 7;
    return [
        'user_id' => rand(1, $ucount),
        'room_id' => rand(1, $rcount),
        'content' => $faker->sentence,
        'is_attachment' => false
    ];
});

