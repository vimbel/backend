<?php

use Illuminate\Database\Seeder;
use Vimbel\Models\{User, Team};

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 10)->create()->each(function($user){
            $user->teams()->save(factory(Team::class)->make());
        });

        User::create([
            'first_name' => 'Tyrell',
            'last_name' => 'Wellick', 
            'email' => 'twellick@ecorp.com',
            'password' => bcrypt('password')
        ]);

         User::create([
            'first_name' => 'User',
            'last_name' => 'Name', 
            'email' => 'test@test.io',
            'password' => bcrypt('password')
        ]);
    }

}
