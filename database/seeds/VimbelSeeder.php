<?php

use Illuminate\Database\Seeder;
use Webpatser\Uuid\Uuid;
use Vimbel\Models\{Message, Room, Team, User};

class VimbelSeeder extends Seeder
{
    public function run()
    {
    	$faker = Faker\Factory::create();

    	Team::create([
            'uuid' => Uuid::generate(5,'Vimbel', Uuid::NS_DNS),
    		'name' => 'Vimbel',
        	'slug' => 'vimbel'
    	]);

        $will = User::create([
            'first_name' => 'Will',
            'last_name' => 'Helms', 
            'email' => 'whelms@vimbel.com',
            'password' => bcrypt('password')
        ]);

        $tavy = User::create([
            'first_name' => 'Tavy',
            'last_name' => 'Walton', 
            'email' => 'twalton@vimbel.com',
            'password' => bcrypt('password')
        ]);

        $bryce = User::create([
            'first_name' => 'Bryce',
            'last_name' => 'Jackson', 
            'email' => 'bjackson@vimbel.com',
            'password' => bcrypt('password')
        ]);

        $dominique = User::create([
            'first_name' => 'Dominique',
            'last_name' => 'Tillman', 
            'email' => 'dtillman@vimbel.com',
            'password' => bcrypt('password')
        ]);

        $will->teams()->attach(1);
        $tavy->teams()->attach(1);
        $bryce->teams()->attach(1);
        $dominique->teams()->attach(1);

        User::create([
            'first_name' => 'Tyrell',
            'last_name' => 'Wellick', 
            'email' => 'twellick@ecorp.com',
            'password' => bcrypt('password')
        ]);

        User::create([
            'first_name' => 'Darlene',
            'last_name' => 'Alderson', 
            'email' => 'dalderson@fsociety.io',
            'password' => bcrypt('password')
        ]);

        User::create([
            'first_name' => 'Elliot',
            'last_name' => 'Alderson', 
            'email' => 'ealderson@fsociety.io',
            'password' => bcrypt('password')
        ]);

        $rooms = ['Design', 'Meetings', 'Deadlines', 'Recruiting', 
        		  'Development', 'Budget', 'Marketing', 'Important'];

		for($i = 0; $i < count($rooms); $i++){
			$room = Room::create([
				'name' => $rooms[$i],
		        'team_id' => 1,
		        'description' => $faker->sentence
			]);
		}

		for($i = 0; $i < 40; $i++){
			Message::create([
				'user_id' => rand(5, User::all()->count()),
		        'room_id' => rand(1, Room::all()->count()),
		        'content' => $faker->sentence,
		        'is_attachment' => false
			]);
		}

        $users = User::all();

        foreach ($users as $user) {
            $user->rooms()->attach(Room::all()->pluck('id')->all());
        }
    }
}
