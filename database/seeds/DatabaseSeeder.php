<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        if(env('VIMBEL_MIGRATION')){
            $this->call(VimbelSeeder::class);
        }
        else{
            $this->call(UsersTableSeeder::class);
            $this->call(RoomsTableSeeder::class);
            $this->call(MessagesTableSeeder::class);
        }
    }
}
