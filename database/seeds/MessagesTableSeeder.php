<?php

use Illuminate\Database\Seeder;
use Vimbel\Models\Message;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Message::class, 12)->create();
    }
}