<?php

use Illuminate\Database\Seeder;
use Vimbel\Models\Room;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Room::class, 7)->create();
    }
}
