<?php 

Route::group(['middleware' => 'cors'], function(){
	Route::group(['middleware' => []], function(){
		Route::group(['prefix' => 'auth'], function(){
			Route::post('register', 'AuthController@store');
		});
		Route::resource('users', 'UsersController');
		Route::resource('messages', 'MessagesController');
		Route::resource('rooms', 'RoomsController');
		Route::group(['prefix' => 'assets'], function(){
			Route::post('web', 'AssetsController@webappAsset');
		});
		
		Route::group(['prefix' => 'uploads'], function(){
			Route::post('attachment', 'UploadsController@attachment');
		});
	});
	Route::group(['prefix' => 'auth'], function(){
		Route::post('login', 'AuthController@authenticate');
	});
});

